# -*- coding: utf8 -*-

import itertools
import numpy as np
import pandas as pd
from os import path
from statsmodels.stats.multitest import multipletests

# Reading in data
data_dir = path.abspath(path.expanduser('data'))
df = pd.read_csv(path.join(data_dir,'chemotaxis_B16.csv'), index_col=False)
df = df.dropna(axis=0,how='any')
df["ECT"].replace({'pLEC':1,'imLEC':2,'MS1':3}, inplace=True)
df = df.groupby('Well').mean()
df["ECT"].replace({1:'pLEC',2:'imLEC',3:'MS1'}, inplace=True)

# Modelling
import statsmodels.api as sm
import statsmodels.formula.api as smf
formula = 'Value ~ C(Day) + Endothelial + DMOG*ECT'
model = smf.ols(formula, df)
fit = model.fit()
summary = fit.summary()
anova_summary = sm.stats.anova_lm(fit, typ=3)

# Print output
print(anova_summary)
print(summary)

# Write output
with open("chemotaxis_B16_modelANOVA.txt", "w") as text_file:
	text_file.write(anova_summary.to_string())
with open("chemotaxis_B16_modelGLM.txt", "w") as text_file:
	text_file.write(summary.as_text())
